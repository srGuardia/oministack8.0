const axios = require("axios");
const Dev = require("../models/dev");

module.exports = {
  async index(req, res) {
    const { user } = req.headers;

    const loggedDev = await Dev.findById(user);

    const users = await Dev.find({
      $and: [
        ///Não tenha o mesmo ID
        { _id: { $ne: user } },
        ///Não aparece quem eu já dei LIKE
        { _id: { $nin: loggedDev.likes } },
        ///Não aparece quem eu já dei DISLIKE
        { _id: { $nin: loggedDev.dislikes } }
      ]
    });

    return res.json(users);
  },

  async store(req, res) {
    const { username } = req.body;

    const userExists = await Dev.findOne({ user: username });

    if (userExists) return res.json(userExists);

    const response = await axios.get(
      `https://api.github.com/users/${username}`
    );

    const { name, bio, avatar_url: avatar } = response.data;

    const dev = await Dev.create({
      name,
      user: username,
      bio,
      avatar
    });
    return res.json(dev);
  }
};
