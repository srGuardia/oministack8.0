const Dev = require("../models/dev");

module.exports = {
  async store(req, res) {
    const { id } = req.params;
    const { user } = req.headers;

    const loggedDev = await Dev.findById(user);
    const targetDev = await Dev.findById(id);

    if (!targetDev)
      return res.status(400).json({ error: "Desenvolvedor não existe!" });

    if (targetDev.likes.includes(loggedDev._id)) {
      console.log("DEU MACTH");
    }

    loggedDev.likes.push(targetDev._id);

    await loggedDev.save();

    return res.json(loggedDev);
  }
};
