const express = require("express");
const DevController = require("./controllers/devController");
const LikeController = require("./controllers/likeController");
const DislikeController = require("./controllers/dislikeController");

const routes = express.Router();

routes.get("/devs", DevController.index);
routes.post("/devs", DevController.store);
routes.post("/devs/:id/likes", LikeController.store);
routes.post("/devs/:id/dislikes", DislikeController.store);

module.exports = routes;
